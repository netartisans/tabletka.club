const gulp = require('gulp'),
  pug = require('gulp-pug2'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  browserSync = require('browser-sync'),
  reload = browserSync.reload;

gulp.task('pug', function () {
  gulp.src('./src/templates/**/*.pug')
    .pipe(pug({
      pretty: true
    }))
  .pipe(gulp.dest('./dist'))
  .pipe(reload({stream:true}))
});

gulp.task('sass', function () {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(reload({stream:true}));
});

gulp.task('images', function () {
  return gulp.src('./src/images/**/*')
    .pipe(gulp.dest('./dist/images/'))
    .pipe(reload({ stream:true }));
});

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: "./dist"
    },
    port: 3000,
    open: true,
    notify: false
  });
});

gulp.task('watch', function () {
  gulp.watch('./src/templates/**/*.pug', ['pug']);
  gulp.watch('./src/sass/**/*.scss', ['sass']);
  gulp.watch('./src/images/**/*', ['images']);
});

gulp.task('cp', function () {
	gulp.src(['./src/js/**/**']).pipe(gulp.dest('./dist/js'));
	gulp.src(['./src/plugins/font-awesome-4.7.0/fonts/**/**']).pipe(gulp.dest('./dist/fonts'));
	gulp.src(['./src/plugins/slick-1.6.0/slick/**/**']).pipe(gulp.dest('./dist/plugins/slick'));
});

gulp.task('default', ['watch','browserSync']);

gulp.task('build', ['pug','sass','images', 'cp']);