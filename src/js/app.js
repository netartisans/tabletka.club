
/*
 By Osvaldas Valutis, www.osvaldas.info
 Available for use under the MIT License
 */

;(function( $, window, document, undefined )
{
	$.fn.doubleTapToGo = function( params )
	{
		if( !( 'ontouchstart' in window ) &&
			!navigator.msMaxTouchPoints &&
			!navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;
		
		this.each( function()
		{
			var curItem = false;
			
			$( this ).on( 'click', function( e )
			{
				var item = $( this );
				if( item[ 0 ] != curItem[ 0 ] )
				{
					e.preventDefault();
					curItem = item;
				}
			});
			
			$( document ).on( 'click touchstart MSPointerDown', function( e )
			{
				var resetItem = true,
					parents	  = $( e.target ).parents();
				
				for( var i = 0; i < parents.length; i++ )
					if( parents[ i ] == curItem[ 0 ] )
						resetItem = false;
				
				if( resetItem )
					curItem = false;
			});
		});
		return this;
	};
})( jQuery, window, document );

jQuery(function($){
	var App = {
		init: function () {
			this.toggleModal();
			this.toggleMenu();
			this.bindEvents();
			this.onResizeScreen();
			this.hideMobileMenu();
			this.onDocumentClick();
		},
		initDrugSlider: function () {
			$('.drugs-slider').slick({
				infinite: true,
				slidesToShow: 5,
				slidesToScroll: 5,
				arrows:false,
				dots: true,
				dotsClass: 'drugs-slider-dots',
				responsive: [
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					}
				]
			});
		},
		bindEvents: function () {
			$('.hamburger, .hide-navigation').on('click', this.toggleMobileMenu.bind(this));
			$('[data-search="mobile-btn"]').on('click', this.onPressMobileSearch.bind(this));
			$('[aria-haspopup="true"]').on('mousedown', this.onTabletMenuHold.bind(this));
			$('[aria-haspopup="true"]').on('click', this.onTabletMenuClick.bind(this));
		},
		onTabletMenuHold: function (e) {
			if(isTablet()) {
				var target  = e.currentTarget;
				var timeout  = setTimeout(function () {
					// console.log("DON'THOLD ME")
				}, 700);
			}
		},
		onTabletMenuClick: function (e) {
			if(isTablet()) {
				e.preventDefault();
			}
		},
		toggleMenu: function() {
			$( '.nav li:has(ul)' ).doubleTapToGo();
		},
		toggleModal: function () {
			$('[data-modal]').click(function (e) {
				var modalId = $(e.target).attr('data-modal');
				var modal = $('#' + modalId);
				if(!modal.length) {
					console.error('Could not find modal with id="#' + modalId + '"!');
				} else {
					modal.toggle();
					$('body').css({
						overflow: 'hidden',
					});
				}
			});
			
			$(window).click(function (e) {
				var target = $(e.target);
				if(target && target.hasClass('modal')) {
					target.css('display', 'none');
					$('body').css({
						overflow: 'auto',
					});
				}
			});
		},
		toggleMobileMenu: function (e) {
			if (e && e.preventDefault) e.preventDefault();
			var navigation = $('.navigation');
			if (navigation.css('display') === 'none') {
				navigation.css('display', 'block');
			} else {
				navigation.css('display', 'none');
			}
		},
		onResizeScreen: function () {
			$(window).resize(function () {
				$('.navigation').removeAttr('style');
				$('[data-logo="container"]').removeClass('hidden-xs');
				$('[data-search="container"]').addClass('hidden-xs');
			});
		},
		hideMobileMenu: function () {
			$(document).click(function (evt) {
				if (isSM()) {
					var target = $(evt.target);
					var navigation = $('.navigation');
					if(
						!(target.is('.navigation') || target.is('.hamburger'))
						&& navigation.css('display') !== 'none'
					) {
						navigation.css('display', 'none');
					}
				}
			});
		},
		onPressMobileSearch: function (evt) {
			if(evt && evt.preventDefault) evt.preventDefault();
			if (isXS()) {
				var search = $('[data-search="container"]'),
						logo = $('[data-logo="container"]');
				if(search.is(':visible')) {
					// submit search form
					alert(' submit search form')
				} else {
					// hide logo & show search form
					logo.addClass('hidden-xs');
					search.removeClass('hidden-xs');
				}
			}
		},
		onDocumentClick: function () {
			$(document).click(function (evt) {
				var target = $(evt.target);
				
				// hide mobile search
				if (isXS()) {
					// var search = $('[data-search="container"]'),
					// 		logo = $('[data-logo="container"]');
					// console.log(evt.target);
					if(
						!(
							target.parent('[data-search="mobile-btn"]').length ||
							target.is('[data-search="mobile-btn"]') ||
							target.is('[data-search="container"]')
						)
					) {
						// hide search form & show logo
						$('[data-logo="container"]').removeClass('hidden-xs');
						$('[data-search="container"]').addClass('hidden-xs');
					}
				}
			});
		}
	};
	
	App.init();
	App.initDrugSlider();
});

function isXS() {
	return window.innerWidth < 768;
}

function isSM() {
	return window.innerWidth >= 768 && window.innerWidth < 992;
}

function isMD() {
	return window.innerWidth >= 992;
}

function isLG() {
	return window.innerWidth >= 1200;
}

function isTablet() {
	return isMobile() && (isMD() || isLG());
}

function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}